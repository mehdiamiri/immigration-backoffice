export default {
    list: [
        {
            title: 'persian',
            value: 'fa-ir',
        },
        {
            title: 'english',
            value: 'en-us',
        },
    ],
    values: {
        'FA-IR': 'FA-IR',
        'EN-US': 'EN-US',
    }
}