export default {
    list: [
        {
            title: {
                'en-us': "saturday",
                'fa-ir': "شنبه"
            },
            value: 'SAT'
        },
        {
            title: {
                'en-us': "sunday",
                'fa-ir': "یکشنبه"
            },
            value: 'SUN'
        },
        {
            title: {
                'en-us': "monday",
                'fa-ir': "دوشنبه"
            },
            value: 'MON'
        },
        {
            title: {
                'en-us': "tuesday",
                'fa-ir': "سه شنبه"
            },
            value: 'TUE'
        },
        {
            title: {
                'en-us': "wednesday",
                'fa-ir': "چهارشنبه"
            },
            value: 'WED'
        },
        {
            title: {
                'en-us': "thursday",
                'fa-ir': "پنجشنبه"
            },
            value: 'THU'
        },
        {
            title: {
                'en-us': "friday",
                'fa-ir': "جمعه"
            },
            value: 'FRI'
        },
    ],
    values: {
        SAT: 'SAT',
        SUN: 'SUN',
        MON: 'MON',
        TUE: 'TUE',
        WED: 'WED',
        THU: 'THU',
        FRI: 'FRI'
    }
};
