export default {
    list: [
        {
            title: {
                en: "PAID",
                fa: "پردداخت شده"
            },
            value: 'PAID'
        },
        {
            title: {
                en: "NOT_PAID",
                fa: "پرداخت نشده"
            },
            value: 'NOT_PAID'
        },
    ],
    values: {
        NOT_PAID: 'NOT_PAID',
        PAID: 'PAID'
    }
};
