import GENDERS from './genders';
import LANGUAGE from './language';
import PAYMENT_STATUS from './paymentStatus';
import WEEK_DAYS from './weekDays';

export default {
  GENDERS,
  LANGUAGE,
  PAYMENT_STATUS,
  WEEK_DAYS
};
