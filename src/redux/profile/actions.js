import API from '../../apis';
import { action, generateErrorAction } from '../actions';
import actionTypes from './actionTypes';

export const fetchUserProfile = _ => {
    const request = () => {
        return dispatch => {
            dispatch(loading());
            return API.profile
                .fetchUserProfile()
                .then(res => dispatch(success(res)))
                .catch(err => {
                    dispatch(
                        generateErrorAction(err, {
                            failure: actionTypes.FETCH_USER_PROFILE_FAILED,
                            reject: actionTypes.FETCH_USER_PROFILE_REJECT
                        })
                    );
                    // throw err;
                });
        };
    };
    const loading = () => action(actionTypes.FETCH_USER_PROFILE_LOADING);
    const success = res => action(actionTypes.FETCH_USER_PROFILE_SUCCESSFULLY, res);
    return request();
};

export const editProfile = profileObj => {
    const request = () => {
        return dispatch => {
            dispatch(loading());
            return API.profile
                .editProfile(profileObj)
                .then(res => dispatch(success(res)))
                .catch(err => {
                    dispatch(
                        generateErrorAction(err, {
                            failure: actionTypes.EDIT_PROFILE_FAILED,
                            reject: actionTypes.EDIT_PROFILE_REJECT
                        })
                    );
                    // throw err;
                });
        };
    };
    const loading = () => action(actionTypes.EDIT_PROFILE_LOADING);
    const success = res => action(actionTypes.EDIT_PROFILE_SUCCESSFULLY, res);
    return request();
};

export const initUserProfile = profileObj => {
    const request = () => {
        return dispatch => {
            dispatch(loading());
            return Promise.all([API.profile.fetchUserProfile(), API.locations.fetchProvinces()])
                .then(res => dispatch(success(res)))
                .catch(err => {
                    dispatch(
                        generateErrorAction(err, {
                            failure: actionTypes.INIT_USER_PROFILE_FAILED,
                            reject: actionTypes.INIT_USER_PROFILE_REJECT
                        })
                    );
                    // throw err;
                });
        };
    };
    const loading = () => action(actionTypes.INIT_USER_PROFILE_LOADING);
    const success = res => action(actionTypes.INIT_USER_PROFILE_SUCCESSFULLY, res);
    return request();
};