import actionTypes from './actionTypes';

const INITIAL_STATE = {
    userProfileLoading: false,
    userProfileFailed: false,
    userProfileError: false,
    userProfileMessage: null,
    userProfile: {},

    editProfileLoading: false,
    editProfileFailed: false,
    editProfileError: false,
    editProfileMessage: null,
    editProfile: {},

    initUserProfileLoading: false,
    initUserProfileFailed: false,
    initUserProfileError: false,
    initUserProfileMessage: null,
    initUserProfile: [],
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case actionTypes.FETCH_USER_PROFILE_LOADING: {
            return {
                ...state,
                userProfileLoading: true,
                userProfileFailed: false,
                userProfileError: false,
                userProfileMessage: null,
                userProfile: {}
            };
        };
        case actionTypes.FETCH_USER_PROFILE_REJECT: {
            return {
                ...state,
                userProfileLoading: false,
                userProfileFailed: false,
                userProfileError: true,
                userProfileMessage: null,
                userProfile: {}
            };
        };
        case actionTypes.FETCH_USER_PROFILE_FAILED: {
            const { message, errors } = action.payload;
            return {
                ...state,
                userProfileLoading: false,
                userProfileFailed: true,
                userProfileError: false,
                userProfileMessage: message,
                userProfile: {}

            };
        };
        case actionTypes.FETCH_USER_PROFILE_SUCCESSFULLY: {
            return {
                ...state,
                userProfileLoading: false,
                userProfileFailed: false,
                userProfileError: false,
                userProfileMessage: null,
                userProfile: { ...action.payload.data }
            };
        };


        case actionTypes.EDIT_PROFILE_LOADING: {
            return {
                ...state,
                editProfileLoading: true,
                editProfileFailed: false,
                editProfileError: false,
                editProfileMessage: null,
                editProfile: {}
            };
        };
        case actionTypes.EDIT_PROFILE_REJECT: {
            return {
                ...state,
                editProfileLoading: false,
                editProfileFailed: false,
                editProfileError: true,
                editProfileMessage: null,
                editProfile: {}
            };
        };
        case actionTypes.EDIT_PROFILE_FAILED: {
            const { message, errors } = action.payload;
            return {
                ...state,
                editProfileLoading: false,
                editProfileFailed: true,
                editProfileError: false,
                editProfileMessage: message,
                editProfile: {}

            };
        };
        case actionTypes.EDIT_PROFILE_SUCCESSFULLY: {
            return {
                ...state,
                editProfileLoading: false,
                editProfileFailed: false,
                editProfileError: false,
                editProfileMessage: null,
                editProfile: { ...action.payload.data }
            };
        };


        case actionTypes.INIT_USER_PROFILE_LOADING: {
            return {
                ...state,
                initUserProfileLoading: true,
                initUserProfileFailed: false,
                initUserProfileError: false,
                initUserProfileMessage: null,
                initUserProfile: []
            };
        };
        case actionTypes.INIT_USER_PROFILE_REJECT: {
            return {
                ...state,
                initUserProfileLoading: false,
                initUserProfileFailed: false,
                initUserProfileError: true,
                initUserProfileMessage: null,
                initUserProfile: []
            };
        };
        case actionTypes.INIT_USER_PROFILE_FAILED: {
            const { message, errors } = action.payload;
            return {
                ...state,
                initUserProfileLoading: false,
                initUserProfileFailed: true,
                initUserProfileError: false,
                initUserProfileMessage: message,
                initUserProfile: []

            };
        };
        case actionTypes.INIT_USER_PROFILE_SUCCESSFULLY: {
            return {
                ...state,
                initUserProfileLoading: false,
                initUserProfileFailed: false,
                initUserProfileError: false,
                initUserProfileMessage: null,
                initUserProfile: [...action.payload]
            };
        };
        default:
            return state;
    }
};
