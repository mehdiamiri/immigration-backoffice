import actionTypes from './actionTypes';
import Farsi from '../../../locales/en-us';

const INITIAL_STATE = {
    currentLanguageLoading: false,
    currentLanguageFailed: false,
    currentLanguageError: false,
    currentLanguageMessage: null,
    currentLanguage: Farsi
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case actionTypes.SET_CURRENT_LANGUAGE_LOADING: {
            return {
                ...state,
                currentLanguageLoading: true,
                currentLanguageFailed: false,
                currentLanguageError: false,
                currentLanguageMessage: null,
                currentLanguage: {}
            };
        };
        case actionTypes.SET_CURRENT_LANGUAGE_REJECT: {
            return {
                ...state,
                currentLanguageLoading: false,
                currentLanguageFailed: false,
                currentLanguageError: true,
                currentLanguageMessage: null,
                currentLanguage: {}
            };
        };
        case actionTypes.SET_CURRENT_LANGUAGE_FAILED: {
            const { message, errors } = action.payload;
            return {
                ...state,
                currentLanguageLoading: false,
                currentLanguageFailed: true,
                currentLanguageError: false,
                currentLanguageMessage: message,
                currentLanguage: {}

            };
        };
        case actionTypes.SET_CURRENT_LANGUAGE_SUCCESSFULLY: {
            console.log('aciton-------->>', action.payload)
            return {
                ...state,
                currentLanguageLoading: false,
                currentLanguageFailed: false,
                currentLanguageError: false,
                currentLanguageMessage: null,
                currentLanguage: action.payload
            };
        };
        default:
            return state;
    }
};
