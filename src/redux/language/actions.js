import API from '../../apis';
import { action, generateErrorAction } from '../actions';
import actionTypes from './actionTypes';

export const setCurrentLanguage = locales => {
    const request = () => {
        return dispatch => {
            dispatch(loading());
            return API.language
                .setCurrentLanguage(locales)
                .then(res => dispatch(success(res)))
                .catch(err => {
                    dispatch(
                        generateErrorAction(err, {
                            failure: actionTypes.SET_CURRENT_LANGUAGE_FAILED,
                            reject: actionTypes.SET_CURRENT_LANGUAGE_REJECT
                        })
                    );
                    // throw err;
                });
        };
    };
    const loading = () => action(actionTypes.SET_CURRENT_LANGUAGE_LOADING);
    const success = res => action(actionTypes.SET_CURRENT_LANGUAGE_SUCCESSFULLY, res);
    return request();
};