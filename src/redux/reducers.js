import { combineReducers } from 'redux';
import languageReducer from './language/reducer';
import authReducer from './auth/reducer';
import profileReducer from './profile/reducer';
import userReducer from './user/reducer';
import appointmentReducer from './appointment/reducer';

export default combineReducers({
  languageReducer,
  authReducer,
  profileReducer,
  userReducer,
  appointmentReducer,
})
