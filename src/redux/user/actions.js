import API from '../../apis';
import { action, generateErrorAction } from '../actions';
import actionTypes from './actionTypes';

export const fetchAllUsers = (pageNumber, itemsPerPage) => {
    const request = () => {
        return dispatch => {
            dispatch(loading());
            return API.user
                .fetchAllUsers(pageNumber, itemsPerPage)
                .then(res => dispatch(success(res)))
                .catch(err => {
                    dispatch(
                        generateErrorAction(err, {
                            failure: actionTypes.FETCH_ALL_USERS_FAILED,
                            reject: actionTypes.FETCH_ALL_USERS_REJECT
                        })
                    );
                    // throw err;
                });
        };
    };
    const loading = () => action(actionTypes.FETCH_ALL_USERS_LOADING);
    const success = res => action(actionTypes.FETCH_ALL_USERS_SUCCESSFULLY, res);
    return request();
};