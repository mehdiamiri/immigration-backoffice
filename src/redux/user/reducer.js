import actionTypes from './actionTypes';

const INITIAL_STATE = {
    usersLoading: false,
    usersFailed: false,
    usersError: false,
    usersMessage: null,
    usersList: [],
    userObject: {},
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case actionTypes.FETCH_ALL_USERS_LOADING: {
            return {
                ...state,
                usersLoading: true,
                usersFailed: false,
                usersError: false,
                usersMessage: null,
                usersList: [],
                userObject: {}
            };
        };
        case actionTypes.FETCH_ALL_USERS_REJECT: {
            return {
                ...state,
                usersLoading: false,
                usersFailed: false,
                usersError: true,
                usersMessage: null,
                usersList: [],
                userObject: {}
            };
        };
        case actionTypes.FETCH_ALL_USERS_FAILED: {
            const { message, errors } = action.payload;
            return {
                ...state,
                usersLoading: false,
                usersFailed: true,
                usersError: false,
                usersMessage: message,
                usersList: [],
                userObject: {}
            };
        };
        case actionTypes.FETCH_ALL_USERS_SUCCESSFULLY: {
            return {
                ...state,
                usersLoading: false,
                usersFailed: false,
                usersError: false,
                usersMessage: null,
                usersList: [...action.payload.data],
                userObject: { ...action.payload },
            };
        };
        default:
            return state;
    }
};
