import actionTypes from './actionTypes';

const INITIAL_STATE = {
    timeSlotsLoading: false,
    timeSlotsFailed: false,
    timeSlotsError: false,
    timeSlotsMessage: null,
    timeSlotObject: {},
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case actionTypes.INSERT_TIME_SLOTS_LOADING: {
            return {
                ...state,
                timeSlotsLoading: true,
                timeSlotsFailed: false,
                timeSlotsError: false,
                timeSlotsMessage: null,
                timeSlotObject: {}
            };
        };
        case actionTypes.INSERT_TIME_SLOTS_REJECT: {
            return {
                ...state,
                timeSlotsLoading: false,
                timeSlotsFailed: false,
                timeSlotsError: true,
                timeSlotsMessage: null,
                timeSlotObject: {}
            };
        };
        case actionTypes.INSERT_TIME_SLOTS_FAILED: {
            const { message, errors } = action.payload;
            return {
                ...state,
                timeSlotsLoading: false,
                timeSlotsFailed: true,
                timeSlotsError: false,
                timeSlotsMessage: message,
                timeSlotObject: {}
            };
        };
        case actionTypes.INSERT_TIME_SLOTS_SUCCESSFULLY: {
            return {
                ...state,
                timeSlotsLoading: false,
                timeSlotsFailed: false,
                timeSlotsError: false,
                timeSlotsMessage: null,
                timeSlotObject: { ...action.payload },
            };
        };
        default:
            return state;
    }
};
