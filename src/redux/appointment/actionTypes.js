export default {
    INSERT_TIME_SLOTS_SUCCESSFULLY: 'INSERT_TIME_SLOTS_SUCCESSFULLY',
    INSERT_TIME_SLOTS_LOADING: 'INSERT_TIME_SLOTS_LOADING',
    INSERT_TIME_SLOTS_REJECT: 'INSERT_TIME_SLOTS_REJECT',
    INSERT_TIME_SLOTS_FAILED: 'INSERT_TIME_SLOTS_FAILED',
}