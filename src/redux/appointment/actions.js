import API from '../../apis';
import { action, generateErrorAction } from '../actions';
import actionTypes from './actionTypes';

export const insertTimeSlots = (timeSlots) => {
    const request = () => {
        return dispatch => {
            dispatch(loading());
            return API.appointment
                .insertTimeSlots(timeSlots)
                .then(res => dispatch(success(res)))
                .catch(err => {
                    dispatch(
                        generateErrorAction(err, {
                            failure: actionTypes.INSERT_TIME_SLOTS_FAILED,
                            reject: actionTypes.INSERT_TIME_SLOTS_REJECT
                        })
                    );
                    // throw err;
                });
        };
    };
    const loading = () => action(actionTypes.INSERT_TIME_SLOTS_LOADING);
    const success = res => action(actionTypes.INSERT_TIME_SLOTS_SUCCESSFULLY, res);
    return request();
};