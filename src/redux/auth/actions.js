import API from '../../apis';
import { action, generateErrorAction } from '../actions';
import actionTypes from './actionTypes';

export const login = loginObj => {
    const request = () => {
        return dispatch => {
            dispatch(loading());
            return API.auth
                .login(loginObj)
                .then(res => dispatch(success(res)))
                .catch(err => {
                    dispatch(
                        generateErrorAction(err, {
                            failure: actionTypes.LOGIN_FAILED,
                            reject: actionTypes.LOGIN_REJECT
                        })
                    );
                    throw err;
                });
        };
    };
    const loading = () => action(actionTypes.LOGIN_LOADING);
    const success = res => action(actionTypes.LOGIN_SUCCESSFULLY, res);
    return request();
};



export const signUp = signUpObj => {
    const request = () => {
        return dispatch => {
            dispatch(loading());
            return API.auth
                .signUp(signUpObj)
                .then(res => dispatch(success(res)))
                .catch(err => {
                    dispatch(
                        generateErrorAction(err, {
                            failure: actionTypes.SIGNUP_FAILED,
                            reject: actionTypes.SIGNUP_REJECT
                        })
                    );
                    throw err;
                });
        };
    };
    const loading = () => action(actionTypes.SIGNUP_LOADING);
    const success = res => action(actionTypes.SIGNUP_SUCCESSFULLY, res);
    return request();
};



export const getMeInfo = () => {
    const request = () => {
        return dispatch => {
            dispatch(loading());
            return API.auth
                .getMeInfo()
                .then(res => dispatch(success(res)))
                .catch(err => {
                    dispatch(
                        generateErrorAction(err, {
                            failure: actionTypes.GET_ME_INFO_FAILED,
                            reject: actionTypes.GET_ME_INFO_REJECT
                        })
                    );
                    throw err;
                });
        };
    };
    const loading = () => action(actionTypes.GET_ME_INFO_LOADING);
    const success = res => action(actionTypes.GET_ME_INFO_SUCCESS, res);
    return request();
};


export const changePassword = (oldPassword, newPassword) => {
    const request = () => {
        return dispatch => {
            dispatch(loading());
            return API.auth
                .changePassword(oldPassword, newPassword)
                .then(res => dispatch(success(res)))
                .catch(err => {
                    dispatch(
                        generateErrorAction(err, {
                            failure: actionTypes.CHANGE_PASSWORD_FAILED,
                            reject: actionTypes.CHANGE_PASSWORD_REJECT
                        })
                    );
                    throw err;
                });
        };
    };
    const loading = () => action(actionTypes.CHANGE_PASSWORD_LOADING);
    const success = res => action(actionTypes.CHANGE_PASSWORD_SUCCESS, res);
    return request();
};
