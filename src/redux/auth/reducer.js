import actionTypes from './actionTypes';

const INITIAL_STATE = {
    loginLoading: false,
    loginFailed: false,
    loginError: false,
    loginMessage: null,
    login: {},

    signUpLoading: false,
    signUpFailed: false,
    signUpError: false,
    signUpMessage: null,
    signUp: {},

    meInfoLoading: false,
    meInfoFailed: false,
    meInfoError: false,
    meInfoMessage: null,
    meInfo: {},

    changePasswordLoading: false,
    changePasswordFailed: false,
    changePasswordError: false,
    changePasswordMessage: null,
    changePassword: {}
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case actionTypes.LOGIN_LOADING: {
            return {
                ...state,
                loginLoading: true,
                loginFailed: false,
                loginError: false,
                loginMessage: null,
                login: {}
            };
        };
        case actionTypes.LOGIN_REJECT: {
            return {
                ...state,
                loginLoading: false,
                loginFailed: false,
                loginError: true,
                loginMessage: null,
                login: {}
            };
        };
        case actionTypes.LOGIN_FAILED: {
            const { message, errors } = action.payload;
            return {
                ...state,
                loginLoading: false,
                loginFailed: true,
                loginError: false,
                loginMessage: message,
                login: {}

            };
        };
        case actionTypes.LOGIN_SUCCESSFULLY: {
            return {
                ...state,
                loginLoading: false,
                loginFailed: false,
                loginError: false,
                loginMessage: null,
                login: { ...action.payload }
            };
        };


        case actionTypes.SIGNUP_LOADING: {
            return {
                ...state,
                signUpLoading: true,
                signUpFailed: false,
                signUpError: false,
                signUpMessage: null,
                signUp: {}
            };
        };
        case actionTypes.SIGNUP_REJECT: {
            return {
                ...state,
                signUpLoading: false,
                signUpFailed: false,
                signUpError: true,
                signUpMessage: null,
                signUp: {}
            };
        };
        case actionTypes.SIGNUP_FAILED: {
            const { message, errors } = action.payload;
            return {
                ...state,
                signUpLoading: false,
                signUpFailed: true,
                signUpError: false,
                signUpMessage: message,
                signUp: {}

            };
        };
        case actionTypes.SIGNUP_SUCCESSFULLY: {
            return {
                ...state,
                signUpLoading: false,
                signUpFailed: false,
                signUpError: false,
                signUpMessage: null,
                signUp: { ...action.payload }
            };
        };



        case actionTypes.GET_ME_INFO_LOADING: {
            return {
                ...state,
                meInfoLoading: true,
                meInfoFailed: false,
                meInfoError: false,
                meInfoMessage: null,
                meInfo: {}
            };
        }
        case actionTypes.GET_ME_INFO_SUCCESS: {
            return {
                ...state,
                meInfoLoading: false,
                meInfoFailed: false,
                meInfoError: false,
                meInfoMessage: null,
                meInfo: { ...action.payload.data }
            };
        }
        case actionTypes.GET_ME_INFO_FAILED: {
            const { message } = action.payload;

            return {
                ...state,
                meInfoLoading: false,
                meInfoFailed: true,
                meInfoError: false,
                meInfoMessage: message,
                meInfo: {}
            };
        }
        case actionTypes.GET_ME_INFO_REJECT: {
            const { message } = action.payload;

            return {
                ...state,
                meInfoLoading: false,
                meInfoFailed: false,
                meInfoError: false,
                meInfoMessage: message,
                meInfo: {}
            };
        }


        case actionTypes.CHANGE_PASSWORD_LOADING: {
            return {
                ...state,
                changePasswordLoading: true,
                changePasswordFailed: false,
                changePasswordError: false,
                changePasswordMessage: null,
                changePassword: {}
            };
        }
        case actionTypes.CHANGE_PASSWORD_SUCCESS: {
            return {
                ...state,
                changePasswordLoading: false,
                changePasswordFailed: false,
                changePasswordError: false,
                changePasswordMessage: null,
                changePassword: { ...action.payload }
            };
        }
        case actionTypes.CHANGE_PASSWORD_FAILED: {
            const { message } = action.payload;

            return {
                ...state,
                changePasswordLoading: false,
                changePasswordFailed: true,
                changePasswordError: false,
                changePasswordMessage: message,
                changePassword: {}
            };
        }
        case actionTypes.CHANGE_PASSWORD_REJECT: {
            const { message } = action.payload;

            return {
                ...state,
                changePasswordLoading: false,
                changePasswordFailed: false,
                changePasswordError: false,
                changePasswordMessage: message,
                changePassword: {}
            };
        }



        default:
            return state;
    }
};
