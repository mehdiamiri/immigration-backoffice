import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react'
import './assets/libraries/fontawesome/fontawesome.css';
import './assets/libraries/fontawesome/regular';
import 'bootstrap/dist/css/bootstrap.css';
import './assets/styles/index.scss';
import 'antd/dist/antd.css';

import App from './App';
import configureStore, { persistor } from './redux/configureStore';


const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <App />
    </PersistGate>
  </Provider>,
  document.getElementById('root')
);
