import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Pagination } from 'antd';
import LoadingOverlay from 'react-loading-overlay';

import ComponentHeader from '../../components/componentHeader';
import * as userActions from '../../redux/user/actions';

const UserManagement = props => {

    const {
        usersLoading,
        totalRecords,
        usersList = [],
        userObject = {},
        currentLanguage
    } = props;

    const [pageNumber, setPageNumber] = useState(0);
    const [itemsPerPage, setItemsPerPage] = useState(10);

    useEffect(() => {
        props.fetchAllUsers(pageNumber, itemsPerPage);
    }, []);

    const handlePageChange = (pageNumber, itemsPerPage) => {
        setPageNumber(pageNumber);
        setItemsPerPage(itemsPerPage);
        props.fetchAllUsers(pageNumber - 1, itemsPerPage)
    };

    return (
        <div
            className='col-12 p-0 m-0'
        >
            <LoadingOverlay
                active={usersLoading}
                spinner
                text={locales('messages.loadingContent')}
            >
                <ComponentHeader
                    name={locales('titles.userManagement')}
                    className='w-100 col-12 p-0 m-0 text-center'
                />

                <table className='table table-striped table-hover table-light table-bordered'>
                    <thead className='thead-dark'>
                        <tr>
                            <th className='text-center text-uppercase'></th>
                            <th className='text-center text-uppercase'>{locales('titles.fullName')}</th>
                            <th className='text-center text-uppercase'>{locales('labels.userName')}</th>
                            <th className='text-center text-uppercase'>{locales('labels.mobileNumber')}</th>
                            <th className='text-center text-uppercase'>{locales('titles.email')}</th>
                            <th className='text-center text-uppercase'>{locales('titles.nationalCode')}</th>
                            <th className='text-center text-uppercase'>{locales('titles.sex')}</th>
                            <th className='text-center text-uppercase'>{locales('labels.description')}</th>
                            <th className='text-center text-uppercase'>{locales('labels.birthDate')}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {usersList.length && usersList.map((item, index) => (
                            <tr
                                key={index}
                            >
                                <td className='text-center'>
                                    <img width='50' height='50' src={`${API_ENDPOINT}file/download/${item.profilePhoto}`} alt="profile-photo" />
                                </td>
                                <td className='text-center'>
                                    {`${item.firstName} ${item.lastName}`}
                                </td>
                                <td className='text-center'>
                                    {`${item.user.username}`}
                                </td>
                                <td className='text-center'>
                                    {`${item.mobileNumber}`}
                                </td>
                                <td className='text-center'>
                                    {`${item.email}`}
                                </td>
                                <td className='text-center'>
                                    {`${item.nationalCode}`}
                                </td>
                                <td className='text-center'>
                                    {`${item.sex}`}
                                </td>
                                <td className='text-center'>
                                    {`${item.description[currentLanguage.direction == 'ltr' ? "en" : "fa"]}`}
                                </td>
                                <td className='text-center'>
                                    {item.birthDate.year}/{item.birthDate.month}/{item.birthDate.day}
                                </td>

                            </tr>
                        ))}
                    </tbody>
                </table>

                <div className='w-100 col-12 p-0 my-2 d-flex justify-content-center align-items-center'>
                    <Pagination
                        defaultPageSize={10}
                        current={pageNumber}
                        pageSize={itemsPerPage}
                        responsive
                        showTitle
                        onChange={handlePageChange}
                        defaultCurrent={1}
                        total={totalRecords}
                        showTotal={_ => locales('labels.totalRecordsAre', { fieldName: totalRecords })}
                        showSizeChanger
                        showQuickJumper
                    />
                </div>
            </LoadingOverlay>
        </div>
    )
}

const mapStateToProps = (state) => {
    const {
        currentLanguage
    } = state.languageReducer;

    const {
        usersLoading,
        usersFailed,
        usersError,
        usersMessage,
        usersList,
        userObject,
    } = state.userReducer;
    return {
        usersLoading,
        usersFailed,
        usersError,
        usersMessage,
        usersList,
        userObject,

        currentLanguage
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        fetchAllUsers: (pageNumber, itemsPerPage) => dispatch(userActions.fetchAllUsers(pageNumber, itemsPerPage))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(UserManagement)