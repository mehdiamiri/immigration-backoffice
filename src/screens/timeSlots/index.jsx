import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Button, TimePicker, Checkbox } from 'antd';

import Header from '../../components/componentHeader';
import Enums from '../../enums';
import * as appointmentActions from '../../redux/appointment/actions';

const { RangePicker } = TimePicker;

const TimeSlots = props => {

    const {
        currentLanguage = {},
        insertTimeSlotsLoading
    } = props;

    const {
        locale
    } = currentLanguage;

    const {
        WEEK_DAYS
    } = Enums;

    const {
        list
    } = WEEK_DAYS;

    const [timesArray, setTimesArray] = useState([]);


    const onTimePickerChanged = (time, day) => {

        const tempTimesArray = [...timesArray];

        const foundDayIndex = tempTimesArray.findIndex(item => item.day == day);

        if (foundDayIndex > -1) {
            tempTimesArray[foundDayIndex].time = time;
        }
        else {
            tempTimesArray.push({
                time,
                day,
            })
        }
        setTimesArray(tempTimesArray);

    };

    const onAvailablityChanged = (event, day) => {

        const { target = {} } = event;
        const { checked } = target;

        let tempTimesArray = [...timesArray];
        const foundDayIndex = tempTimesArray.findIndex(item => item.day == day);
        if (foundDayIndex > -1) {
            tempTimesArray[foundDayIndex].isAvailale = checked;
        }
        console.log('checked', checked, 'day', day, 'times', tempTimesArray)
        setTimesArray(tempTimesArray);
    };

    const onSubmit = _ => {
        props.insertTimeSlots(timesArray);
    };

    return (
        <div
            className='col-12 p-0 m-0 bg-white'
        >

            <Header
                name={locales('titles.timeSlot')}
            />
            <section
                className='col-12 p-0 m-0 d-flex flex-column align-items-center justify-content-center'
            >
                <table
                    className='table table-light table-striped table-hover table-bordered col-10'
                >
                    <thead
                        className='thead-dark'
                    >
                        <tr>
                            <th
                                className='text-center align-middle text-capitalize'
                            >
                            </th>
                            <th
                                className='text-center align-middle text-capitalize'
                            >
                                {locales('titles.time')}
                            </th>
                            <th
                                className='text-center align-middle text-capitalize'
                            >
                                {locales('titles.isAvailable')}
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {list.map((item, index) => (
                            <tr
                                key={index}
                            >
                                <td
                                    className='text-center align-middle text-capitalize'
                                >
                                    {item.title[locale]}
                                </td>
                                <td
                                    className='text-center align-middle text-capitalize'
                                >
                                    <RangePicker
                                        format='HH:mm'
                                        onChange={time => onTimePickerChanged(time, item.value)}
                                        value={timesArray.find(times => times.day == item.value)?.time}
                                    />
                                </td>
                                <td
                                    className='text-center align-middle text-capitalize'
                                >
                                    <Checkbox
                                        disabled={timesArray.every(times => times.day != item.value)}
                                        onChange={event => onAvailablityChanged(event, item.value)}
                                    />
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </section>
            <Button
                type='primary'
                className='text-center font-large text-uppercase  m-4'
                onClick={onSubmit}
                size='large'
                loading={insertTimeSlotsLoading}
            >
                {locales('titles.submit')}
            </Button>
        </div>
    )
};

const mapStateToProps = ({
    languageReducer,
    appointmentReducer
}) => {

    const {
        insertTimeSlotsLoading
    } = appointmentReducer;

    const {
        currentLanguage
    } = languageReducer;

    return {
        currentLanguage,
        insertTimeSlotsLoading
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        insertTimeSlots: timeSlots => dispatch(appointmentActions.insertTimeSlots(timeSlots))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(TimeSlots);