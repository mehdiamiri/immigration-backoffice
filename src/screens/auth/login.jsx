import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import * as authActions from "../../redux/auth/actions";
import * as profileActions from "../../redux/profile/actions";

import { validator, domManipulator } from "../../utils";
import styles from "./styles.scss";

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userName: "",
            userNameError: "",

            password: "",
            passwordError: "",

            submitted: false,
            errors: [],
        };
    }

    isComponentMounted = false;

    componentDidMount() {
        this.isComponentMounted = true;
        if (this.isComponentMounted) {
            domManipulator.scrollToTop();
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (
            this.props.currentLanguage.title !== prevProps.currentLanguage.title &&
            this.state.submitted
        ) {
            this.validateForm();
        }
    }

    componentWillUnmount() {
        this.isComponentMounted = false;
    }

    handlePasswordChange = (event) => {
        const { value } = event.target;
        this.setState({ password: value, passwordError: "" });
    };

    handleUserNameChange = (event) => {
        const { value } = event.target;
        this.setState({ userName: value, userNameError: "" });
    };

    validateForm = (_) => {
        return new Promise((resolve, reject) => {
            const { userName, password } = this.state;

            let userNameError,
                passwordError,
                isUserNameValid,
                isPassowrdValid,
                firstErrorElementId = "",
                errors = [];

            if (!userName) {
                userNameError = locales("errors.fieldNeeded", {
                    fieldName: locales("labels.userName"),
                });
                firstErrorElementId = "userName";
                isUserNameValid = false;
            } else if (userName && !validator.isValidInstagramUserName(userName)) {
                userNameError = locales("errors.invalidField", {
                    fieldName: locales("labels.userName"),
                });
                firstErrorElementId = "password";
                isUserNameValid = false;
            } else {
                userNameError = "";
                isUserNameValid = true;
            }

            if (!password) {
                passwordError = locales("errors.fieldNeeded", {
                    fieldName: locales("labels.password"),
                });
                isPassowrdValid = false;
            } else if (
                password &&
                !validator.hasMinLength(password, { minLength: 8 })
            ) {
                passwordError = locales("errors.hasNoValidatedLength", {
                    fieldName: locales("labels.password"),
                    fieldNameLength: "8",
                });
                isPassowrdValid = false;
            } else {
                passwordError = "";
                isPassowrdValid = true;
            }

            if (isPassowrdValid && isUserNameValid) {
                this.setState({ errors: [] }, () => {
                    resolve(true);
                });
            } else {
                // domManipulator.scrollIntoView(firstErrorElementId)
                errors.push(userNameError, passwordError);
                this.setState({ userNameError, passwordError, errors }, () => {
                    resolve(false);
                });
            }
        });
    };

    onFormSubmitted = async (event) => {
        event.preventDefault();
        this.setState({ submitted: true });
        if (!this.validateForm()) {
            return;
        }

        const { userName, password } = this.state;
        let loginObj = {
            username: userName,
            password,
        };
        this.props.login(loginObj).then((_) => {
            this.props.fetchUserProfile().then((_) => {
                this.props.getMeInfo().then((_) => {
                    this.props.history.push("/");
                });
            });
        });
    };

    render() {
        const { currentLanguage } = this.props;
        const {
            userName,
            password,
            userNameError,
            passwordError,
            errors,
        } = this.state;

        return (
            <>

                <header
                    className={`col-12 p-4 ${styles.headerBg} d-flex justify-content-start align-items-center`}
                >
                    <div className={`${styles.paddingHorizontal}`}>
                        <Link to={"/"} className="text-dark mx-2">
                            <span>{locales("titles.motorun")}</span>
                        </Link>
                        <span className={`${styles.bar}`}></span>
                        <span className="text-blue mx-2 text-uppercase">
                            {locales("titles.login")}
                        </span>
                    </div>
                </header>
                <div
                    className={`${styles.sliderBackgroundImage} d-flex justify-content-center align-items-center ${styles.firstRowProductContainer}`}
                >
                    <section
                        className={`${styles.blueBackground} position-relative ${styles.largePaddingTop}`}
                    >
                        <div className={`${styles.oLogo}`}>O</div>

                        <h4 className="font-weight-bolder text-white text-center">
                            {currentLanguage.direction == "ltr" ? (
                                <>
                                    {locales("titles.welcome")}{" "}
                                    <span className="text-white">{locales("titles.moto")}</span>
                                    <span style={{ color: "#959eeb" }} className="">
                                        {locales("titles.run")}
                                    </span>
                                </>
                            ) : (
                                    <>
                                        {locales("titles.to")}{" "}
                                        <span className="text-white">{locales("titles.moto")}</span>
                                        <span style={{ color: "#959eeb" }} className="">
                                            {locales("titles.run")}{" "}
                                            <span className="text-white">
                                                {locales("titles.welcome")}
                                            </span>
                                        </span>
                                    </>
                                )}
                        </h4>

                        <form
                            onSubmit={this.onFormSubmitted}
                            className={`w-100 py-5 d-flex justify-content-center align-items-center d-flex flex-column`}
                        >
                            <input
                                id="userName"
                                placeholder={locales("labels.userName")}
                                onChange={this.handleUserNameChange}
                                value={userName}
                                type="text"
                                className={`px-2 w-75 ${styles.inputContainer} py-4 my-2 ${userNameError ? "border-white" : ""
                                    }`}
                            />
                            {/* {userNameError && <small className='text-white'>{userNameError}</small>} */}

                            <input
                                id="password"
                                placeholder={locales("labels.password")}
                                onChange={this.handlePasswordChange}
                                value={password}
                                type="password"
                                className={`px-2 w-75 ${styles.inputContainer} py-4 my-2 ${passwordError ? "border-white" : ""
                                    }`}
                            />
                            {/* {passwordError && <small className='text-white'>{passwordError}</small>} */}

                            <div className="d-flex align-items-center justify-content-between w-75">
                                <div className={`pointer ${styles.forgetText}`}>
                                    {locales("labels.rememberMe")}
                                </div>
                                <Link to="/forget-password" className={`${styles.forgetText}`}>
                                    {locales("labels.forgetPassword")}
                                </Link>
                            </div>

                            <div className="col-12 d-flex justify-content-end align-items-center my-5">
                                {errors.length && errors.some((item) => item) ? (
                                    <div
                                        className={`text-white mx-2 col-8 d-flex justify-content-end text-${currentLanguage.direction == "ltr" ? "left" : "right"
                                            } align-items-center`}
                                    >
                                        <i className="fal fa-exclamation-triangle mx-1" />{" "}
                                        {errors.filter((item) => item)[0]}
                                    </div>
                                ) : null}
                                <div
                                    className={`${styles.mobileLoginButtonMargin} col-4 d-flex justify-content-start align-items-center`}
                                >
                                    <button
                                        type="submit"
                                        className={`${styles.mobileLoginButtonWidth} text-uppercase font-weight-bolder btn btn-light bg-white text-dark w-75  py-3`}
                                    >
                                        {locales("titles.login")}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </section>

                    <section
                        className={`${styles.grayBackground} ${styles.largePaddingTop} position-relative py-5`}
                    >
                        <div className={`${styles.rLogo}`}>R</div>

                        <h4 className="font-weight-bolder text-dark text-center">
                            <span className="text-uppercase">{locales("titles.login")}</span>{" "}
                            <span className="">
                                {locales("titles.loginUsingSocialMedia")}
                            </span>
                        </h4>

                        <div
                            className={`d-flex justify-content-center align-items-center ${styles.iconMargins}`}
                        >
                            <div className="mx-3 pointer">
                                <img
                                    className={`${styles.googleIcon} rounded-circle`}
                                    src={require("../../assets/icons/google.png")}
                                    alt="google"
                                />
                            </div>
                            <div className="mx-3 pointer">
                                <img
                                    className={`${styles.googleIcon} rounded-circle`}
                                    src={require("../../assets/icons/twitter.png")}
                                    alt="twitter"
                                />
                            </div>
                            <div className="mx-3 pointer">
                                <img
                                    className={`${styles.googleIcon} rounded-circle`}
                                    src={require("../../assets/icons/facebook2.png")}
                                    alt="facebook"
                                />
                            </div>
                        </div>

                        <h6 className="text-muted font-weight-bolder text-center my-4">
                            {locales("titles.notRegistered")}
                        </h6>
                        <Link to="/signUp">
                            <h4
                                className={`main-blue-text font-weight-bolder text-center my-4 ${styles.textDecoration}`}
                            >
                                {locales("titles.createAccount")}
                            </h4>
                        </Link>
                    </section>
                </div>
                <div className="d-none d-lg-block">
                </div>
            </>
        );
    }
}

const mapStateToProps = (state) => {
    const {
        currentLanguageLoading,
        currentLanguageError,
        currentLanguageMessage,
        currentLanguageFailed,
        currentLanguage,
    } = state.languageReducer;

    return {
        currentLanguageLoading,
        currentLanguageError,
        currentLanguageMessage,
        currentLanguageFailed,
        currentLanguage,
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        login: (loginObj) => dispatch(authActions.login(loginObj)),
        fetchUserProfile: (_) => dispatch(profileActions.fetchUserProfile()),
        getMeInfo: (_) => dispatch(authActions.getMeInfo()),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login);
