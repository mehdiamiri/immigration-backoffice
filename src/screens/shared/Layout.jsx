import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Layout as AntdLayout } from 'antd';

import AppRouter from '../../routes/router';
import SideBar from '../../components/sideBar';
import Footer from '../../components/footer';
import styles from './styles.scss';

const { Footer: AntdFooter, Sider, Content } = AntdLayout;

class Layout extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showHeader: false
    }
  }


  componentDidMount() {
    document.addEventListener('scroll', () => {
      this.setState({ showHeader: window.pageYOffset <= 130 });
    })
  }

  render() {
    const { showHeader } = this.state;

    return (
      <AntdLayout className={`${styles.minHeight}`} hasSider>
        <AntdLayout>
          <Sider>
            <SideBar {...this.props} />
          </Sider>
          <Content>
            <AppRouter />
          </Content>
        </AntdLayout>
        {/* <AntdFooter>
            <Footer />
          </AntdFooter> */}
      </AntdLayout>
    );
  }
}



const mapStateToProps = (state) => {

  const {
    currentLanguageLoading,
    currentLanguageError,
    currentLanguageMessage,
    currentLanguageFailed,
    currentLanguage
  } = state.languageReducer;

  return {
    currentLanguageLoading,
    currentLanguageError,
    currentLanguageMessage,
    currentLanguageFailed,
    currentLanguage
  }
};

export default connect(mapStateToProps)(Layout)
