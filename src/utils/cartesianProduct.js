
const elementsCreatorFunc = (a, b) => [].concat(...a.map(a => b.map(b => [].concat(a, b))));


export const cartesian = (...args) => args[1] ? cartesian(elementsCreatorFunc(args[0], args[1]), args[2]) : args[0];



