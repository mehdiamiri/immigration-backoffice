export function convertEnumValueToTitle(enumData, enumValue) {
    if (enumValue === undefined || enumValue === null || enumValue === '') {
        return '';
    }
    const item = enumData.find(x => x.value === enumValue);
    return item && item.title;
}

export function convertEnumValueToMaxValue(enumData, enumValue) {
    if (!enumValue) {
        return '';
    }
    return enumData.find(x => x.value === enumValue).maxValue;
}
