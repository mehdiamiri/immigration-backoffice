import React, { useState } from 'react';
import styles from './styles.scss';

const Input = props => {

    const {
        type = 'text',
        onChange = () => { },
        placeHolder = locales('titles.typeYourSearch'),
        value = '',
        className = '',
        handleUpArrowChange = () => { },
        handleDownArrowChange = () => { }
    } = props;

    const textInput = () => {
        return <>
            <div className={`${styles.selectContainer} d-flex pointer ${className}`}>

                <input
                    placeholder={placeHolder}
                    type='text'
                    value={value}
                    onChange={event => onChange(event)}
                    className={`${styles.inputContainer} border-0 d-flex justify-content-start align-items-center`}
                />

                <div
                    className={`${styles.arrowContainer}`}>
                    <i className='fal fa-search text-white' />
                </div>

            </div>
        </>
    };

    const numberInput = () => {
        return (
            <>
                <div className={`${styles.numberInputParent} d-flex pointer ${className}`}>

                    <input
                        placeholder={placeHolder}
                        type='text'
                        value={value}
                        onChange={event => onChange(event)}
                        className={`${styles.numberInputContainer} main-red-bg text-white border-0 d-flex justify-content-start align-items-center`}
                    />

                    <div
                        className={`bg-white d-flex flex-column border w-100 align-items-center justify-content-center`}>
                        <i onClick={event => handleUpArrowChange(event)}
                            className={`py-1 fal fa-chevron-up text-dark border-bottom w-100 text-center ${styles.arrow}`}
                        />
                        <i onClick={event => handleDownArrowChange(event)}
                            className={`py-1 fal fa-chevron-down text-dark text-center w-100 ${styles.arrow}`} />
                    </div>

                </div>
            </>
        )
    };

    switch (type) {
        case 'text': {
            return textInput();
        }
        case 'number': {
            return numberInput();
        }
        default:
            return textInput();
    }
}
export default Input