import React from 'react';
import { connect } from 'react-redux';

import * as languageActions from '../../redux/language/actions';
import * as $ from 'jquery';
import styles from './styles.scss';






export const ForwardBorderedButton = connect((state) => {

    const {
        currentLanguageLoading,
        currentLanguageError,
        currentLanguageMessage,
        currentLanguageFailed,
        currentLanguage
    } = state.languageReducer;

    return {
        currentLanguageLoading,
        currentLanguageError,
        currentLanguageMessage,
        currentLanguageFailed,
        currentLanguage
    }
})(props => {


    const {
        color = 'dark',
        bordered = true,
        containerStyle = 'p-4',
        textStyle = 'font-large mx-1',
        iconStyle = 'font-xlarge',
        fullWidthRectangle = false,
        tailStyle = { color: 'white', backgroundColor: 'white' },
        text = locales('titles.next'),
        textTailWidth = 105,
        onClick = () => { },
        rectangleClassName = ''
    } = props;

    $('#textTail').css('width', '10px !important')

    if (props.currentLanguage.direction == 'ltr')
        return (
            <div onClick={() => onClick()}
                className={`pointer d-flex align-items-center justify-content-between ${styles.borderedButtonWidth} ${containerStyle}`}
            >

                <div id='textTail' className={`border-${color} ${styles.textTail}`} style={{ ...tailStyle }}></div>
                <div className={`text-${color} ${textStyle} text-uppercase`}>{text}</div>

                <div className='d-flex align-items-center mx-2'>
                    <div id='borderBottomArrowTail' className={`border-${color} ${styles.borderedButtonArrowTail}`} style={{ width: textTailWidth }}></div>
                    <i className={`fas fa-caret-right ${iconStyle} text-${color}`} />
                </div>

                {bordered && <div className={`${rectangleClassName} ${styles.rectangledBorder} ${fullWidthRectangle ? 'w-100' : ''}`}></div>}
            </div>
        )
    return (
        <div onClick={() => onClick()}
            className={`pointer d-flex align-items-center justify-content-between ${styles.borderedButtonWidth} ${containerStyle}`}
        >

            <div className='d-flex align-items-center mx-2'>
                <i className={`fas fa-caret-right ${iconStyle} text-${color}`} />
                <div id='borderBottomArrowTail' className={`border-${color} ${styles.borderedButtonArrowTail}`} style={{ width: textTailWidth }}></div>
            </div>

            <div className={`text-${color} ${textStyle} text-uppercase`}>{text}</div>
            <div id='textTail' className={`border-${color} ${styles.textTail}`} style={{ ...tailStyle }}></div>

            {bordered && <div className={`${rectangleClassName} ${styles.rectangledBorder} ${fullWidthRectangle ? 'w-100' : ''}`}></div>}
        </div>
    )
}
)








export const BackwardBorderedButton = connect((state) => {

    const {
        currentLanguageLoading,
        currentLanguageError,
        currentLanguageMessage,
        currentLanguageFailed,
        currentLanguage
    } = state.languageReducer;

    return {
        currentLanguageLoading,
        currentLanguageError,
        currentLanguageMessage,
        currentLanguageFailed,
        currentLanguage
    }
})(props => {


    const {
        color = 'white',
        text = locales('titles.prev'),
        containerStyle = '',
        tailStyle = {},
        textStyle = 'font-large',
        fullWidthRectangle = false,
        rectangleClassName = '',
        iconStyle = 'font-xlarge',
        onClick = () => { },
        bordered = true,
        textTailWidth = 220
    } = props;


    if (props.currentLanguage.direction == 'ltr')
        return (
            <div onClick={() => onClick()}
                className={`${containerStyle} p-4 pointer d-flex align-items-center justify-content-between ${styles.borderedButtonWidth}`}>
                {bordered && <div className={`${rectangleClassName} ${styles.backwardRectangledBorder} ${fullWidthRectangle ? 'w-100' : ''}`}></div>}
                <div className='d-flex align-items-center mx-2'>
                    <i className={`fas fa-caret-left ${iconStyle} text-${color}`} />
                    <div className={`border-${color} ${styles.backBorderedButtonArrowTail}`} style={{ ...tailStyle }}></div>
                </div>
                <div className={`mx-1 text-${color} ${textStyle} text-uppercase`}>{text}</div>
                <div className={`border-${color} ${styles.backTextTail}`} style={{ width: `${textTailWidth}px !important` }}></div>


            </div>
        );
    return (
        <div onClick={() => onClick()}
            className={`${containerStyle} p-4 pointer d-flex align-items-center justify-content-between ${styles.borderedButtonWidth}`}>
            {bordered && <div className={`${rectangleClassName} ${styles.backwardRectangledBorder} ${fullWidthRectangle ? 'w-100' : ''}`}></div>}

            <div className={`border-${color} ${styles.backTextTail}`} style={{ width: `${textTailWidth}px !important` }}></div>
            <div className={`mx-1 text-${color} ${textStyle} text-uppercase`}>{text}</div>

            <div className='d-flex align-items-center mx-2'>
                <div className={`border-${color} ${styles.backBorderedButtonArrowTail}`} style={{ ...tailStyle }}></div>
                <i className={`fas fa-caret-left ${iconStyle} text-${color}`} />
            </div>
        </div>
    )
}
)
