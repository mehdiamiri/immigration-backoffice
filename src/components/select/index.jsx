import React, { useState, useEffect } from 'react';
import styles from './styles.scss';
import * as $ from 'jquery';

const Select = props => {

    const { options = [], onChange = () => { }, containerClassName = '', barStyle = {}, inputStyle = {},
        placeHolder = locales('titles.selectAnItem') } = props;

    let [optionsContainerTop, setOptionsContainerTop] = useState(-10);
    let [showOptionsContainer, setShowOptionsContainer] = useState(false);
    let [selectedValue, setSelectedValue] = useState({});

    const setSelectedOption = option => {
        onChange(option);
        setSelectedValue(option);
        setShowOptionsContainer(false);
    };

    useEffect(() => {
        setOptionsContainerTop($('#optionsContainer').height());
    }, [])

    return (
        <>
            <select className='d-none'></select>

            <div className={`${styles.selectContainer} ${containerClassName} d-flex pointer`}>

                <div
                    style={{ ...inputStyle }}
                    onClick={() => setShowOptionsContainer(!showOptionsContainer)}
                    className={`${styles.inputContainer} d-flex justify-content-start align-items-center`}>
                    {selectedValue.title || placeHolder}
                </div>

                <div
                    style={{ ...barStyle }}
                    onClick={() => setShowOptionsContainer(!showOptionsContainer)}
                    className={`${styles.arrowContainer}`}>
                    <i className='far fa-chevron-down text-white' />
                </div>

                <ul
                    onClick={() => setShowOptionsContainer(false)}
                    id='optionsContainer' className={`${styles.optionsContainer} ${styles.listWithoutDot} ${showOptionsContainer ? 'visible' : 'hidden'}`}
                    style={{ top: -optionsContainerTop }}
                >
                    {options.map((option, index) => (
                        <li className='pointer my-2'
                            onClick={() => setSelectedOption(option)} key={index}
                        >{option.title}</li>
                    ))}
                </ul>
            </div>
        </>
    )
}
export default Select