import React, { Component } from 'react';
import { connect } from 'react-redux';
import styles from './styles.scss';

class Footer extends Component {


    render() {
        const { currentLanguage } = this.props;

        return (
            <>

                <div className={`${styles.firstRowProductContainer} col-12 pt-5 m-0 ${styles.footerContainer} d-flex justify-content-start align-items-center`}>

                    <div className={`col-lg-3 col-sm-12 p-0 d-flex flex-column flex-wrap aling-items-center justify-content-center`}>
                        <h4 className={`text-white text-uppercase text-${currentLanguage.direction == 'ltr' ? 'left' : 'right'} font-weight-bolder`}>{locales('titles.aboutmotoRun')}</h4>
                        <div style={{ lineHeight: '22px' }} className={`${styles.paragraphText} font-weight-medium text-${currentLanguage.direction == 'ltr' ? 'left' : 'right'}`}>
                            {locales('titles.lookingFor')}<br />{locales('titles.smoothesToReact')}<br />{locales('titles.speedAndCurise')}<br />
                            {locales('titles.competitors')}<br />{locales('titles.welcomeTomotoRun')}<br />

                            <div className='my-3'>
                                <i className={`fal fa-clock text-white font-xlarge ${currentLanguage.direction == 'ltr' ? 'mr-2' : 'ml-2'}`} />{locales('titles.workingPlan')}<br />
                                <span className={`${styles.marginHorizontal} text-center`}>
                                    {locales('titles.thuPlan')}
                                </span>
                            </div>
                        </div>
                    </div>


                    <div className={`col-lg-3 col-sm-12 p-0 d-flex flex-column flex-wrap aling-items-center justify-content-center`}>
                        <h4 className={`text-white font-weight-bolder text-${currentLanguage.direction == 'ltr' ? 'left' : 'right'} text-uppercase`}>
                            {locales('labels.ourServices')}</h4>
                        <div className={`font-weight-medium ${styles.paragraphText} text-${currentLanguage.direction == 'ltr' ? 'left' : 'right'}`}>
                            <div className={`${styles.footerHoverColor} py-2`}>
                                {locales('labels.chemicalProject')}
                            </div>
                            <div className={`${styles.footerHoverColor} py-2`}>
                                {locales('labels.miningEngineering')}
                            </div>
                            <div className={`${styles.footerHoverColor} py-2`}>
                                {locales('labels.constructionEngineering')}
                            </div>
                            <div className={`${styles.footerHoverColor} py-2`}>
                                {locales('labels.weldingEngineering')}
                            </div>
                            <div className={`${styles.footerHoverColor} py-2`}>
                                {locales('labels.constructionEngineering')}
                                {locales('titles.spaceProgram')}
                            </div>
                        </div>
                    </div>

                    <div className={`col-lg-3 col-sm-12 p-0 pb-2 d-flex flex-column flex-wrap aling-items-center justify-content-center`}>
                        <h4 className={`text-white text-uppercase font-weight-bolder text-${currentLanguage.direction == 'ltr' ? 'left' : 'right'}`}>{locales('titles.tehranOffice')}</h4>
                        <div className={`${styles.paragraphText} text-${currentLanguage.direction == 'ltr' ? 'left' : 'right'}`}   >
                            <div className={`text-lowercase font-weight-medium ${styles.footerHoverColor} py-2`}>
                                <i className='fal fa-map-marker-alt px-2 text-white font-xlarge' />7398 {locales('titles.footerAddress')}<br /><span className={`${styles.marginHorizontal}`}> 242 {locales('titles.footerAddressContinue')}</span>
                            </div>
                            <div className={`${styles.footerHoverColor} py-2 text-lowercase font-weight-medium`}>
                                <i className='fal fa-phone-volume px-2 text-white font-xlarge text-lowercase font-weight-medium' />
                                + (123) 124-567-8901
                                <br /><span className={`${styles.numberMarginHorizontal}`}>+ (123) 124-567-8901</span>
                            </div>
                            <div className={`${styles.footerHoverColor} py-2 text-lowercase font-weight-medium`}>
                                <i className='fal fa-envelope px-2 text-white font-xlarge text-lowercase font-weight-medium' />
                               carrier@qodeinteractive.com
                                <br /><span className={`${styles.emailMarginHorizontal}`}>grand@qodeinteractive.com</span>
                            </div>
                        </div>
                    </div>

                    <div className={`col-lg-3 col-sm-12 p-0 pb-4 d-flex flex-column flex-wrap aling-items-center justify-content-center`}>
                        <h4 className={`text-white text-uperrcase font-weight-bolder text-${currentLanguage.direction == 'ltr' ? 'left' : 'right'}`}>{locales('titles.ourLocations')}</h4>
                        <img className="alignnone size-medium wp-image-179"
                            src="https://grandprix.qodeinteractive.com/wp-content/uploads/2019/09/footer-img-5-300x171.png"
                            alt="b" width="300" height="171"
                        />
                    </div>

                </div>
                <section className={`${styles.bgDark} col-12 p-4 font-smaller d-flex justify-content-center align-items-center`}>
                    <p>
                        {locales('titles.copyRight')} 2019 © <span className={`${styles.footerHoverColor}`}>{locales('titles.codeInterActives')}</span>
                    </p>
                </section>
            </>
        )
    }
}


const mapStateToProps = (state) => {

    const {
        currentLanguageLoading,
        currentLanguageError,
        currentLanguageMessage,
        currentLanguageFailed,
        currentLanguage
    } = state.languageReducer;

    return {
        currentLanguageLoading,
        currentLanguageError,
        currentLanguageMessage,
        currentLanguageFailed,
        currentLanguage
    }
};

export default connect(mapStateToProps)(Footer);
