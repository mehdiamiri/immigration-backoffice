import React from 'react';
import styles from './styles.scss';

const CheckBox = props => {

    const {
        labelClassName = '',
        onChange = () => { },
        checked = false,
        label = '',
        color = '#ED1D24'
    } = props;

    return (
        <div>
            <label>
                <input
                    checked={checked}
                    type="checkbox"
                    className='d-none'
                    onChange={onChange}
                />
                <div className={`d-flex align-items-center justify-content-center ${styles.checkboxContainer} border rounded`}>
                    <i style={{ color: color }} className={`${styles.checkIcon} far fa-check ${checked ? 'visible' : 'hidden'}`} />
                </div>
                <div className={`${labelClassName}`}>{label}</div>
            </label>
        </div>
    )
}

export default CheckBox;