import React from 'react';
import './styles.scss';

const Spin = ({ spinning, children }) => {
  return (
    <>
      <div className={spinning ? 'loadingContainer' : ''}>
        {children}
        {spinning && <img src={require('../../assets/icons/spinner.gif')} className={'loadingSpinner'} />}
      </div>
    </>
  );
};

export default Spin;
