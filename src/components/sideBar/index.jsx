import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Menu, Radio } from 'antd';
import {
    AppstoreOutlined,
} from '@ant-design/icons';
import { connect } from 'react-redux';

import * as languageActions from '../../redux/language/actions';

const { SubMenu } = Menu;

class SideBar extends Component {
    state = {
        mode: 'vertical',
        theme: 'light',
        currentLanguage: this.props.currentLanguage.locale
    };



    onCurrentLanguageChanged = event => {
        this.setState({ currentLanguage: event.target.value })
        this.props.setCurrentLanguage(event.target.value)
    };

    render() {
        const {
            currentLanguage
        } = this.state;

        return (
            <Menu
                mode={this.state.mode}
                theme={this.state.theme}
            >
                {/* <SubMenu className='text-uppercase' key="sub1" icon={<AppstoreOutlined />}
                    title={locales('labels.product')}> */}
                <Menu.Item key="3">
                    <Link
                        className='text-capitalize'
                        to='/user'>
                        {locales('titles.userManagement')}
                    </Link>
                </Menu.Item>
                <Menu.Item key="4">
                    <Link
                        className='text-capitalize'
                        to='/time-slots'>
                        {locales('titles.timeSlot')}
                    </Link>
                </Menu.Item>
                {/* </SubMenu> */}
                <Radio.Group onChange={this.onCurrentLanguageChanged} value={currentLanguage}>
                    <Radio className='text-uppercase' checked={currentLanguage == 'fa-ir'} value={'fa-ir'}>{locales('titles.persian')}</Radio>
                    <Radio className='text-uppercase' checked={currentLanguage == 'en-us'} value={'en-us'}>{locales('titles.english')}</Radio>
                </Radio.Group>
            </Menu>
        );
    }
}

const mapStateToProps = (state) => {
    return {
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setCurrentLanguage: language => dispatch(languageActions.setCurrentLanguage(language))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SideBar)