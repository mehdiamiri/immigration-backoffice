import React from 'react';
import { validator } from '../../utils';

class Rating extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            half: false,
            hoveredIndex: props.starsCount * 2.0 || 10.0,
            clicked: false,
            loaded: false
        }
    }


    componentDidMount() {

        if (!!this.props.defaultRate) {

            let { defaultRate, starsCount } = this.props;
            let isDefaultRateInteger = validator.isInt(defaultRate);

            this.setState({
                hoveredIndex: isDefaultRateInteger ? starsCount - defaultRate : starsCount - 0.5 - defaultRate,
                half: !isDefaultRateInteger
            });
        }

    }


    handleHover = (event, index) => {
        let { width } = event.target.getBoundingClientRect();
        let { offsetX } = event.nativeEvent;

        if (offsetX <= Math.round(width / 2)) {
            this.setState({ half: true, hoveredIndex: index });
        }
        else {
            this.setState({ half: false, hoveredIndex: index })
        }
    }

    renderStars = index => {
        let { hoveredIndex, half } = this.state;
        let { color = 'danger' } = this.props;
        hoveredIndex = Math.round(hoveredIndex);

        if (hoveredIndex < index) {
            return `fas fa-star text-${color}`;
        }
        else if (hoveredIndex > index) {
            return 'fal fa-star';
        }
        else if (half) {
            return `fas fa-star-half-alt text-${color}`;
        }
        else {
            return `fas fa-star text-${color}`;
        }
    };

    calculateRate = index => {
        let { starsCount } = this.props;
        return this.state.half ? starsCount - index - 0.5 : starsCount - index
    }

    handleStarClick = (index) => {

        let { name = '', renderedParentComponentIndex } = this.props;
        this.setState({ clicked: true, hoveredIndex: index }, () => this.renderStars(index));

        this.props.handleStarClick(this.calculateRate(index), name, renderedParentComponentIndex);
    };

    handleLeave = (index) => {
        if (!this.state.clicked) {
            this.setState({ hoveredIndex: this.props.starsCount * 2 || 10 });
        }
        else {
            this.handleStarClick(index)
        }
    };

    render() {
        let {
            starsCount = 5,
            containerStyle = {},
            size = 'xlarge',
            containerClassName = '',
            disable = true
        } = this.props;

        let stars = []

        for (let i = 0; i < starsCount; i++) {
            stars.push(i);
        };

        return (
            <div
                style={containerStyle}
                className={`${containerClassName}`}>
                {
                    stars.map((_, index) => (
                        <i
                            key={index}
                            title={this.calculateRate(index)}
                            onClick={() => !disable && this.handleStarClick(index)}
                            onMouseLeave={() => !disable && this.handleLeave(index)}
                            onMouseMove={event => !disable && this.handleHover(event, index)}
                            className={`${this.renderStars(index)} font-${size} pointer`}
                        />

                    ))
                }
            </div>
        )
    }
}
export default Rating