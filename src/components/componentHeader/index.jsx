import React from 'react';

export default ({ name = '', className = '' }) => {
    return (
        <h3
            className={`${className} text-uppercase font-giant font-weight-bold text-center py-2`}
        >
            {name}
        </h3>
    )
}