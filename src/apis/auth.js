import cookies from 'js-cookie';
import {
    requester
} from '../utils';


export const login = loginObj => {
    return new Promise((resolve, reject) => {
        requester
            .fetchAPI({
                route: `auth/login_backoffice`,
                method: 'POST',
                data: loginObj,
                withAuth: true
            })
            .then(result => {
                console.log('login successfully', result);
                cookies.set('accessToken', result.payload.data.accessToken, { expires: 0.03125 });
                cookies.set('refreshToken', result.payload.data.refreshToken, { expires: 60 });
                resolve(result.payload);
            })
            .catch(err => {
                console.log('login error', err);
                reject(err);
            });
    });
};

export const signUp = signUpObj => {
    return new Promise((resolve, reject) => {
        requester
            .fetchAPI({
                route: `auth/signup`,
                method: 'POST',
                data: signUpObj,
                withAuth: true
            })
            .then(result => {
                console.log('res', result)
                resolve(result.payload);
            })
            .catch(err => {
                reject(err);
            });
    });
};


export const getMeInfo = () => {
    return new Promise((resolve, reject) => {
        requester
            .fetchAPI({
                route: `auth/me`,
                method: 'GET',
                withAuth: true
            })
            .then(result => {
                resolve(result.payload);
            })
            .catch(err => {
                reject(err);
            });
    });
};

export const changePassword = (oldPassword, newPassword) => {
    return new Promise((resolve, reject) => {
        requester
            .fetchAPI({
                route: `auth/change-password`,
                method: 'POST',
                withAuth: true,
                data: {
                    oldPassword,
                    newPassword
                }

            })
            .then(result => {
                resolve(result.payload);
            })
            .catch(err => {
                reject(err);
            });
    });
};
