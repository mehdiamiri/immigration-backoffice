import languagesData from '../../locales';

export const setCurrentLanguage = (locale = 'fa-ir') => {
    return new Promise((resolve, reject) => {
        languagesData.setActiveLanguage(locale).then(result => {
            resolve(result);
        }).catch(error => {
            reject(error);
        })
    });
};
