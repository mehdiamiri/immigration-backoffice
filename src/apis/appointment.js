import {
    requester
} from '../utils';

export const insertTimeSlots = timeSlots => {
    return new Promise((resolve, reject) => {
        requester
            .fetchAPI({
                route: `appointments/insert/time-slots`,
                method: 'POST',
                data: timeSlots,
                withAuth: true
            })
            .then(result => {
                resolve(result.payload);
            })
            .catch(err => {
                reject(err);
            });
    });
};
