import {
    requester
} from '../utils';

export const fetchAllUsers = (pageNumber, itemsPerPage) => {
    return new Promise((resolve, reject) => {
        requester
            .fetchAPI({
                route: `profile/all/${pageNumber}/${itemsPerPage}`,
                method: 'GET',
                withAuth: true
            })
            .then(result => {
                resolve(result.payload);
            })
            .catch(err => {
                reject(err);
            });
    });
};
