import * as language from './language';
import * as auth from './auth';
import * as profile from './profile';
import * as user from './user';
import * as appointment from './appointment';

export default {
  language,
  auth,
  profile,
  user,
  appointment
};
