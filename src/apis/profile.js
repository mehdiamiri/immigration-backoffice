import {
    requester
} from '../utils';

export const fetchUserProfile = id => {
    return new Promise((resolve, reject) => {
        requester
            .fetchAPI({
                route: `profile/me`,
                method: 'GET',
                withAuth: true
            })
            .then(result => {
                resolve(result.payload);
            })
            .catch(err => {
                reject(err);
            });
    });
};

export const editProfile = profileObj => {
    return new Promise((resolve, reject) => {
        requester
            .fetchAPI({
                route: `profile/me`,
                method: 'PUT',
                data: profileObj,
                withAuth: true
            })
            .then(result => {
                resolve(result.payload);
            })
            .catch(err => {
                reject(err);
            });
    });
};