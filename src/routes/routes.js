import Users from '../screens/users';
import TimeSlots from '../screens/timeSlots';

export const routes = [
  {
    path: '/user',
    component: Users,
    exact: true
  },
  {
    path: '/time-slots',
    component: TimeSlots,
    exact: true
  },
];
