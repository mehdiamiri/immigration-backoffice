import React, { Fragment } from 'react';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { routes } from './routes';
import './router.scss';
const routeList = [...routes];

class Router extends React.Component {
  render() {
    return (
      <Switch>
        {routeList.map((route, routeIndex) => {
          return <Route key={routeIndex} exact={!!route.exact} path={route.path} component={route.component} />;
        })}
      </Switch>
    );
  }
}


export default (withRouter(Router));
