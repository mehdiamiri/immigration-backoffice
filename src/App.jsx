import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';

import * as languageActions from './redux/language/actions';
import locales from '../locales';

import Layout from './screens/shared/Layout';
import NotFoundPage from './screens/shared/NotFoundPage';
import Login from './screens/auth/login';

global.locales = () => { };
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedLanguage: this.props.currentLanguage.locale
    };


  }

  componentDidMount() {
    this.props.setCurrentLanguage(this.state.selectedLanguage).then(result => {
      this.setState({ selectedLanguage: result.payload.locale });
    });
    global.locales = locales.localize;
  }

  render() {

    const { currentLanguage, meInfo = {} } = this.props;
    const { direction } = currentLanguage;

    const {
      username
    } = meInfo;
    return (
      <div className="col-12 main-content p-0 m-0"
        style={{ fontFamily: direction == 'ltr' ? 'Rajdhani' : 'IranSans', direction }}
      >
        <BrowserRouter>
          <Switch>
            <Route path="/login" component={Login} />
            <Route path="/404" component={NotFoundPage} />
            {!username ?
              <Route path="/login" component={Login} />
              :
              <Route path="/" component={Layout} />
            }
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}


const mapStateToProps = (state) => {

  const {
    currentLanguageLoading,
    currentLanguageError,
    currentLanguageMessage,
    currentLanguageFailed,
    currentLanguage
  } = state.languageReducer;

  const {
    meInfo
  } = state.authReducer;

  return {
    currentLanguageLoading,
    currentLanguageError,
    currentLanguageMessage,
    currentLanguageFailed,
    currentLanguage,

    meInfo
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    setCurrentLanguage: locale => dispatch(languageActions.setCurrentLanguage(locale))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(App);
