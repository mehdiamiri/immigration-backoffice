import faIR from './fa-ir';
import enUs from './en-us';

const languages = {
  [faIR.locale]: faIR,
  [enUs.locale]: enUs,
};

let activeLanguage = null;

const localize = (key, params = {}) => {

  if (!activeLanguage)
    activeLanguage = faIR;

  let result = activeLanguage.strings[key];

  for (const param in params) {
    let val = params[param];
    if (val === undefined || val === null) {
      val = `'خالی'`;
    }
    result = result.replace(`{${param}}`, val);
  }

  return result;
};

const setActiveLanguage = locale => {
  return new Promise((resolve, reject) => {
    activeLanguage = languages[locale];
    if (!!activeLanguage) {
      resolve(activeLanguage)
    }
    else {
      reject(false);
    }
  });
};

export default {
  localize,
  setActiveLanguage
};
